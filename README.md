# Auto Multiple Choice (AMC) - How To

> **Auto Multiple Choice** (AMC) is a piece of software that can help you creating and managing [multiple choice](https://en.wikipedia.org/wiki/Multiple_choice) questionnaires, with automated marking and [LaTeX](https://en.wikipedia.org/wiki/LaTeX) support. AMC is a free software, distributed under the [GPLv2+ licence](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html).

Website: https://www.auto-multiple-choice.net  
Official Documentation: https://www.auto-multiple-choice.net/doc.en

[[_TOC_]]

## Tips and Tricks / FAQ

More important informations are found here -> [Tips and Tricks / FAQ](FAQ.md).

## Installation

AMC ist already installed on **all our Fedora Linux devices**. To use it with other devices (also private ones) you will need to do it through VPN+ThinLinc on a Linux remote clients ([read more](https://blogs.ethz.ch/isgdmath/tango)).

## How to start AMC

Click on "Activities" (in the top-left corner) and search for "amc" and press ENTER:

![images/start.png](images/start.png)

## Using AMC

### Change some default values

Click on the "Preferences" icon (see the image below) and go through all settings, in particular change "Marking" values according to your needs:

![images/settings.png](images/settings.png)

### Prepare a multiple choice exam

Click on the icon right to the "Open"-button, enter a project name (ex. "MyFirstExam") and click on the button "New project":

![images/new-project.png](images/new-project.png)

On the next step select "Empty" and click on "Forward":

![images/new-project-source.png](images/new-project-source.png)

Now you are ready to begin writing the exam. Click on "Edit source file" and an almost empty file will be created and opened in your default (see above) LaTeX editor:

![images/latex-editor.png](images/latex-editor.png)

Substitute the LaTeX code with the following example:

```tex
\documentclass[a4paper]{article}

% packages

\usepackage{automultiplechoice}
\usepackage{amssymb}
\usepackage{csvsimple}
\usepackage[linewidth=1pt]{mdframed}
\usepackage{fix-cm}

% settings

\setlength{\parindent}{0pt}
\def\multiSymbole{$\blacklozenge$}

\begin{document}

% define all the questions and answers

\element{geography}{
  \begin{question}{geo01} % IMPORTANT: the id (geo01} must be unique in this file
    What is the capital of Egypt?
    \begin{choiceshoriz}
      \correctchoice{Cairo}
      \wrongchoice{Caracas}
      \wrongchoice{Cayenne}
      \wrongchoice{Conakry}
    \end{choiceshoriz}
  \end{question}
}

\element{geography}{
  \begin{question}{geo002} % IMPORTANT: the id (geo02} must be unique in this file
    What is the capital of Ireland?
    \begin{choiceshoriz}
      \correctchoice{Dublin}
      \wrongchoice{Dili}
      \wrongchoice{Djibouti}
      \wrongchoice{Dakar}
    \end{choiceshoriz}
  \end{question}
}

\element{history}{
  \begin{questionmult}{hist001} % IMPORTANT: the id (hist001} must be unique in this file
    Which of the following events are taking place during the year 1901?
    \begin{choices}
      \correctchoice{Funeral of Queen Victoria in London}
      \correctchoice{Official end of the Caste War of Yucat\'an}
      \wrongchoice{King George of Greece becomes absolute monarch of Crete}
      \wrongchoice{The first line of the Paris M\'etro is opened}
    \end{choices}
  \end{questionmult}
}

\element{history}{
  \begin{questionmult}{hist002} % IMPORTANT: the id (hist002} must be unique in this file
    Which of the following events are taking place during the year 1850?
    \begin{choices}
      \correctchoice{American Express is founded by Henry Wells \& William Fargo}
      \wrongchoice{Napoleon Bonaparte crosses the Alps and invades Italy}
      \wrongchoice{Kwang-su becomes emperor of China}
      \wrongchoice{First horse-drawn omnibuses established in London}
    \end{choices}
  \end{questionmult}
}

\newcommand{\Exam}{
  \onecopy{1}{

	% header

    % teacher, exam title, date/time
    \begin{minipage}[b]{10cm}
      \bf Prof. Dr. Peter Muster \\ Exam on Geography \& History, D-XYZ \\ March 25, 2099, 10:00--12:00
    \end{minipage}
    \hfill
    % id
    \begin{minipage}[b]{4cm}
      \hfill\fontsize{80}{100}{\selectfont{\id}}
    \end{minipage}
    \vspace{1cm}

    % student name
    \hfill\namefield{\Huge\name}\hfill
    \vspace{5mm}

    % legi nr.
    \hfill{\Large Legi Nr. \legi}\hfill
    \vspace{10mm}

    % important notes / exam rules
    \begin{mdframed}
      \centering  

      {\bf Important notes / Exam rules}

      \begin{itemize}
        \item This is an example with 2 groups of questions about ``geography'' and ``history''.
          Inside each group the questions are shuffled and all answers are shuffled as well.
        \item Moreover questions marked with \multiSymbole{} allow zero, one or several correct
          choices among the proposed answers. For the other questions  there is one and only
          one correct answer.
        \item No special scoring strategy hast been defined. The default scale for a simple question
          (only one allowed answer) gives one point for a good response and no point in the other cases.
          The default scaling for a multiple question gives a point for every checked box, either good or not
          (good box checked or wrong box not checked).
      \end{itemize}

    \end{mdframed}

    \vspace{10mm}

    \cleargroup{all}

    \section*{Questions about Geography}
    \shufflegroup{geography}
    \insertgroup{geography}

    \section*{Questions about History}
    \shufflegroup{history}
    \insertgroup{history}

    \AMCassociation{\id}
  }
 }

\csvreader[head to column names]{./students.csv}{}{\Exam}

\end{document}
```

and save in the same directory (for instance in `MC-Projects/MyFirstExam`) a file called `students.csv`  with the following content:

```csv
id,legi,name
1,18-123-456,Alex MEYER
2,18-000-000,Susanne BÜHLER
3,17-987-654,Simon SCHMIED
4,18-444-444,Andrea ROSSI
```

Now you are ready to compile the exam in your LaTeX editor. You should get 4 personalized pages, one for each student, with a "DRAFT" watermark and a note in blue at the bottom of each page:

![images/exams-draft.png](images/exams-draft.png)

### Print the multiple choice exam

To print the multiple choice exam, you will need to leave your LaTeX editor, go back to the AMC program and follow the following 3 steps:

**Step 1 - Update the documents:** Click on the button "Update documents". In this way the exam will be compiled again by producing the personalized questions and also the personalized answer sheets. If you are happy with the result (see "Questions") go the next step, otherwise edit the source again a repeat this step.

**Step 2 - Do the layout detection:** Click on "Layout detection". This is an important step: all pages of the exam are analized and the exact position of every element (checkboxes) will be identified and saved for the future automatic correction. You can check whether the layouts have been correctly detected: the red checkboxes should be located over the black checkboxes.

**Step 3 - Print the exam:** Click on "Print papers" to print the exam. Type **CTRL+a** to select all pages. If you need to print the file differently (for instance via the "Print + Publish Services" aka "Reprozentrale") you will find the file here:

```bash
MC-Projects/MyFirstExam/DOC-sujet.pdf
```

![images/print.png](images/print.png)

### Import the scaned solved exams

After the examination you will have to scan all the exams (in any order) and import them into AMC. The best results are obtained scanning the exams in TIFF (not PDF) or JPG. Follow then these steps:

1. save all the files for instance on the Desktop
2. go to the "Data capure"-tab
3. click on "Automatic"
4. navigate to the Desktop and select all the scans
5. select the Option "Different answer sheets"
6. and click on "OK"

![images/data-capture.png](images/data-capture.png)

The automatic data capture is now completed. You can analyze the quality of the data capture with some indicators. **MSE** represents positioning gap for the four corner marks; great value means abnormal page distortion (scan again?). Great values of **sensitivity** are seen when darkness ratio is very close to the threshold for some boxes.

### Check the imported results

> See also this [alternative way](FAQ.md#manual-check-the-imported-results), with recognized checkboxes and scanned file overlapped.

Check now on **all** pages if the hacked/filled checkboxes have been recognized correctly and do some manual adjustments. As follow:

1. select the first line in the table "Diagnosis"
2. click in "Zooms" - you will see on the top all checkboxes that have been recognized as "unchecked" and on the bottom the "checked" ones
3. adjust mistakes by "drag and drop" and confirm by clicking on "Apply"
4. go to the next page by clicking on "Forward"

![images/adjust-mistakes.png](images/adjust-mistakes.png)

Wenn this process is finished you are ready for the next one: Marking and students identification

### Marking and students identification

In this step you are going to set the marks and identifying the students, which will work automatically:

1. go to the "Marking"-tab
2. click on "Mark" - the Mean should appears few seconds later
3. click on "Set file" and select your `students.csv`
4. set the "Primary key" to "id" and "Code name for automatic association" to "Pre-association"
5. click on "Automatic" - all students should be recognized automatically
6. click on "Manual" just to check the result - all the names should match!

![images/marks.png](images/marks.png)

### Reports

In this last step you can export the marks into Excel/LibreOffice and create personalized annotated and corrected exams.

**Export the marks**

<img src="images/export-marks.png" class="img-responsive">

**Create personalized annotated and corrected exams**

![images/annotated-and-corrected-exams.png](images/annotated-and-corrected-exams.png)
