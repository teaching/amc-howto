# Install AMC on a Mac

On the [AMC Website](https://www.auto-multiple-choice.net/download-macosx.en) you will find 2 documentations to install AMC on a Mac: one with [MacPorts](http://www.macports.org/) and one with [Homebrew](https://brew.sh/). MacPorts is actually our preferred package manager but unfortunately the installation does no seem to work very well and crashes sometimes. On the other side Homebrew is very popular and the installation is very easy and seems to work properly.

## Prerequisites

**Admin rights** (for `sudo` commands) are needed for the installation. This should already be the case if you have a MacBook.

**You should have some familiarity with the Mac Terminal application** since you'll need to use it to install Homebrew. The Terminal application is located in the Utilities folder in the Applications folder.

Install the Command Line Tools (via Terminal) as follow:

```bash
sudo xcode-select --install
```

## Install Homebrew

In this example we are going to install Homebrew in your Home directory so that you don't need any admin rights, but you can choose any other location. Enter the following command in a Terminal:

```bash
bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

## Install AMC

Just enter in a Terminal the command:

```bash
brew install --build-from-source maelvls/amc/auto-multiple-choice
```

This will first install some Homebrew core files, then all dependencies needed by AMC and finally AMC self. The whole process will take approximatively 10-20 minutes (depending on your internet connection).

Then add the special `automultiplechoice.sty` to your latex installation, as follow:

```bash
sudo auto-multiple-choice latex-link
```

And at the end remove the huge cache containing no more needed files:

```bash
rm -rf $HOME/Library/Caches/Homebrew
```

## Running AMC

Again, in a Terminal, enter the following command:

```bash
auto-multiple-choice
```
