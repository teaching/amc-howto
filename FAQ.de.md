# Tips and Tricks / FAQ

[[_TOC_]]

## Importierte Resultate manuell prüfen

Ein guter weg um **alle** Seiten dahingehend zu überprüfen, ob alle Checkboxen korrekt erkannt wurden, und diese ggf. anzupassen ist:

1. Die Prüfungen wie hier beschrieben importieren: "[Import the scaned solved exams](README.md#import-the-scaned-solved-exams)"
2. Klick auf "Manual"
3. Die rechte Seite von "Original" auf "Scan" umstellen.

Dann erscheint die gescannte Version mit den erkannten Checkboxen.

![images/adjust-mistakes2.png](images/adjust-mistakes2.png)

## Liste von Studenten via CSV importieren

**Achtung:** im Beispiel (siehe [README.md](README.md)) wird auch das LaTeX-Paket `csvsimple` verwendet. Dieses hat einen wichtigen Nachteil, den man kennen muss, falls man es einsetzt: falls ein Wert in der gegebenen `csv`-Datei ein Komma enthält (escaped in ""), dann wird diese Zeile von `csvsimple` einfach ignoriert. Kommas tauchen z.B. in Adressen von Studierenden auf, wenn die Studierendenliste aus eDoz exportiert wird. Dies führt dazu, dass die LaTeX-Datei zwar ohne Warnungen oder Fehler kompiliert, aber einige Seiten einfach fehlen.

Man kann das Problem umgehen, wenn man einen anderen Trenner anstatt das Komma verwendet, z.B. ";" oder den Tabulator (auch bekannt als "Tab Separated Values", vermutlich die bessere Wahl) und im LaTeX File so einbindet (siehe `separator=tab`):

```latex
\csvreader[head to column names,separator=tab]{./students.csv}{}{\Exam}
```

## Beispiel mit Legi-Nr. ankreuzen

Statt individuelle Antwortblätter kann man ebenfalls eines nehmen und dieses kopieren. \
Der Student könnte mit Boxen z.B. die Legi-Nummer angeben. Siehe Screenshot hier unten. \
Dies funktioniert sicher gut, wenn es sich um ein separates, einseitiges Antwortblatt handelt.

![images/legi-nr.png](images/legi-nr.png)

## Automatische Identifikation via Legi-Nr. anstatt Name

Man kann im `csv` File (siehe [README.md](README.md)) die Kolonne "name" mit der Legi-Nr. ersetzen, z.B.:

```csv
id,legi,name,fullname
1,18-123-456,18-123-456,Alex MEYER
2,18-000-000,18-000-000,Susanne BÜHLER
3,17-987-654,17-987-654,Simon SCHMIED
4,18-444-444,18-444-444,Andrea ROSSI
```
dann wird bei der automatischen Identifikation von eingescannte Prüfungen die Legi-Nr. verwendet.

## Prüfung einseitg oder doppelseitig ausdrucken?

Wenn die Prüfung doppelseitig ausgedruckt wird, besteht die Gefahr, das die Farbe der angekreuzten Antworten durch das Blatt druckt (z.B. mit einer wasserfester Farbe, oder dicken Filzstiften) und somit das Resultat auf der Rückseite fälschen könnte. Darum:

1. entweder einseitig drucken
2. oder auf dickeres Papier drucken.

## Checkboxen rot anstatt schwarz

Mit der LaTeX Option `\AMCboxStyle{color=red}` kann man die Checkboxen rot generieren und dann dem Programm in den Einstellungen (> Scans > Erase red from scans) sagen, dass es Rot ignorieren soll. \
Somit werden deutlich weniger Ränder der Checkboxen versehentlich als Kreuz erkannt (geht natürlich nur, wenn nicht in rot angekreuzt wird).

![images/red-checkboxes.png](images/red-checkboxes.png)

## Automatic Data Capture

Folgende Einstellungen sind wichtig:

![images/darkness-threshold.png](images/darkness-threshold.png)

### Darkness threshold

Die untere Schwelle bestimmt, wie sensitiv die Maschine korrigiert. Man muss aufpassen, falls man Buchstaben in den Checkboxen hat: in diesen Fall sollte den Wert 0.5 sein.

Die besseren Resultate bekommt man, wenn die Checkboxen ausgefüllt werden und ein Wert von 0.5 beim Parameter "Darkness threshold" gesetzt ist.

Aus der Dokumentation:

> If black proportion is greater that this value, the box will be considered as being ticket. If students have been told to darken the box entirely, one can chose a value of 0.5. If students have been told instead to tick the boxes, a value around 0.15 seems to be appropriate.

### Upper darkness threshold

Die obere Schwelle soll umbedingt 1 sein, denn wir wollen ja eine ausgefüllte Checkbox ebenfalls als Antwort akzeptieren.

## Korrigierte Prüfungen via E-Mail verschicken

Es ist möglich, voll automatisiert, E-Mails an den Studierenden mit der Korrigierte Prüfung als Anhang zu schicken.

### Voraussetzung

Das Versenden von E-Mails ist auf 500 Mails pro Tag limitiert. Falls mehr E-Mails versendet werden sollen, muss ein Tag im voraus eine temporäre Erhöhung der Limite beim Servicedesk (servicedesk@id.ethz.ch) beantragt werden. \
**Achtung:** fall eine Kopie per Cc konfiguriert ist, dann muss die Anzahl E-Mails x2 multiplziert werden, z.B. bei 600 Prüfungen wären 1'200 E-Mails (plus Reserve, z.B. 10%).

Die E-Mail Adresse von den Studierenden soll im CSV File vorhanden sein, z.B.:

```csv
id,legi,name,email
1,18-123-456,Alex MEYER,alexm@student.ethz.ch
2,18-000-000,Susanne BÜHLER,susanneb@student.ethz.ch
3,17-987-654,Simon SCHMIED,simons@student.ethz.ch
4,18-444-444,Andrea ROSSI,andrear@student.ethz.ch
```

### Konfiguration

AMC muss mindestens wie folgt konfigurieren werden (der `SMTP user` ist dein ETH Userkonto und das `SMTP password` ist dein ETH Passwort für E-Mail):

![images/email-settings.png](images/email-settings.png)

### E-Mails verschicken

Korrigierte Prüfungen generieren:

![images/email-send-1.png](images/email-send-1.png)

und E-Mail verschicken:

![images/email-send-2.png](images/email-send-2.png)
