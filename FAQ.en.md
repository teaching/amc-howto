# Tips and Tricks / FAQ

[[_TOC_]]

## Manual check the imported results

Another efficient way to check on **all** pages if the hacked/filled checkboxes have been recognized correctly and do some manual adjustments. is following:

1. After the step "[Import the scaned solved exams](README.md#import-the-scaned-solved-exams)"
2. Click on "Manual"
3. Change on the right-side from "Original" to "Scan"

The you will see the scanned version with the recognized checkboxes:

![images/adjust-mistakes2.png](images/adjust-mistakes2.png)

## Import List of Students via CSV

**Attention:** The Example (see [README.md](README.md)) uses also a LaTeX-Package called `csvsimple`. Said package has an important **drawback** that you have to know to use it. It handles the the comma (`,`) in a peculiar way. **It ignores lines that contain a comma.** \
Adresses of students thet were exported with eDoz usually contain commas. \
This can be mitigated by setting the `separator` to a semicolon ('`;`') or tab. \
So either:
```latex
\csvreader[head to column names,separator=;]{./students.csv}{}{\Exam}
```
or:
```latex
\csvreader[head to column names,separator=tab]{./students.csv}{}{\Exam}
```
## Reusing Answering Sheets with Legi-Nr.

Instead of using individual answering sheets you can also just copy an existing one where the stundent has to provide a Legi-Nr in some form or another. \
This is particularly handy if it is a onesided, seperate answering sheet.

![images/legi-nr.png](images/legi-nr.png)

## Identification via Legi-Nr. instead of Name

You can replace the values in the column "name" with the legi-nr in the CSV file (see [README.md](README.md)) \

```csv
id,legi,name,fullname
1,18-123-456,18-123-456,Alex MEYER
2,18-000-000,18-000-000,Susanne BÜHLER
3,17-987-654,17-987-654,Simon SCHMIED
4,18-444-444,18-444-444,Andrea ROSSI
```

that way, the legi-nr will automatically used when identifying scanned exams.

## Notes on one sided (simplex) and double sided (duplex) printing.

While it is commendable to use duplex printing to save some paper, it has drawbacks. \
If a student uses a rather wet or agressive marker for example, it can percolate through the paper and mess with the scanner. \
So either: \

    a) Print one sided (simplex)
    b) Use thick paper for two sided (duplex) prints

## Red Checkboxes insted of Black Ones

You can generate red checkboxes with the LaTeX option `\AMCboxStyle{color=red}`. \
You'll also need to setup the scan settings accordingly: Options -> Scans -> Erase red from cans. \
Of course, this only works if the checkmarks aren't made with a red markers.

![images/red-checkboxes.png](images/red-checkboxes.png)

## Automatic Data Capture

The following settings are important for automatic data capture to work properly.

![images/darkness-threshold.png](images/darkness-threshold.png)

### Darkness threshold

The darkness threshold states, how sensitive the machine should react.

Excerpt from the documentation:

> If black proportion is greater that this value, the box will be considered as being ticket. If students have been told to darken the box entirely, one can chose a value of 0.5. If students have been told instead to tick the boxes, a value around 0.15 seems to be appropriate.

### Upper darkness threshold

Leave the upper darkness threshold at 1, we want to count fully filled out boxes.

## Send Marked Exams via E-Mail

It's possible to send out marked exams out to students via e-mail.

### Requirements

#### E-Mail limitations

There is a limit of **500** e-mails per account per day. \
In case you need to send out more e-mails, you can request to raise that limit temporarily by writing an e-mail to our [servicedesk](mailto:servicedesk@id.ethz.ch) a day in advance. \
It's also worth mentioning that if you want to CC those e-mails to someone, the number of actual sent out mails would **double**.

Of course the e-mail address needs to be supplied in the CSV file. For example:
```csv
id,legi,name,email
1,18-123-456,Alex MEYER,alexm@student.ethz.ch
2,18-000-000,Susanne BÜHLER,susanneb@student.ethz.ch
3,17-987-654,Simon SCHMIED,simons@student.ethz.ch
4,18-444-444,Andrea ROSSI,andrear@student.ethz.ch
```

### Configuration

AMC has to be configured accordingly.
You need to provide your ETH username as your `SMTP user` as well as your ETH password as your `SMTP password`.

![images/email-settings.png](images/email-settings.png)

### Generate Marked Exams

Generate marked exams as shown here:

![images/email-send-1.png](images/email-send-1.png)

and then send them out via e-mail as shown here:

![images/email-send-2.png](images/email-send-2.png)
