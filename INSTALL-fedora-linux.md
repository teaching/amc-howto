# Install MAC on Fedora Linux

## Install using the official Fedora repository

[Patrice Kadionik](http://doc.fedora-fr.org/wiki/Utilisateur:PatriceKadionik`)
maintains the package `auto-multiple-choice` in his Fedora repository.

To use this repository, type the following command (here for Fedora 36, but
this also works starting from Fedora 23) as root:

```bash
dnf install http://kadionik.enseirb-matmeca.fr/fedora/eddy33-release-36.rpm
```

Then you can install all necessary packages:

```bash
dnf install evince libreoffice
dnf install auto-multiple-choice
```

## Create your own RPM for Fedora

The official way to build your own RPM is to use one of the pre combiled
sources (see the "precom" files in the [download area](https://download.auto-multiple-choice.net/))
and execute this command:

```bash
rpmbuild -tb auto-multiple-choice_*_precomp.tar.gz
```

Unfortunately this do not work with Fedora 36, since some files that are referred in the `.spec` file are missing. For this reason we use the `.spec` file that has been used to create the official RPM, gently provided by Patrice Kadionik and slighly modified: [auto-multiple-choice-1.5.2-x86_64.spec](fedora/auto-multiple-choice-1.5.2-x86_64.spec).

To create the RPM proceed as follow on a Fedora 36:

```bash
# Creade directories for rpmbuild, as root
mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS}
# Download the .spec file
cd ~/rpmbuild/SPECS
curl -O https://gitlab.math.ethz.ch/teaching/amc-howto/-/raw/master/fedora/auto-multiple-choice-1.5.2-x86_64.spec
# Download the prebuild file
cd ~/rpmbuild/SOURCES
curl -O https://download.auto-multiple-choice.net/auto-multiple-choice_1.5.2_precomp.tar.gz
# Build RPM (-> will be saved to ~/rpmbuild/RPMS)
rpmbuild -bb ~/rpmbuild/SPECS/auto-multiple-choice-1.5.2-x86_64.spec
```

Or using docker:

```bash
# Pull fedora
docker pull fedora:36
# Create a persistent volume
mkdir ~/RPMS
# Login to the container
docker run -v ~/RPMS:/root/rpmbuild/RPMS -it fedora:36 bash
# Install all (minimal) dependencies ~ 1000 RPMs ;-)
dnf -y install redhat-lsb-core rpm-build desktop-file-utils netpbm-devel texlive perl-macros \
    fontconfig-devel opencv-devel gcc-c++ cairo-devel poppler-devel poppler-glib-devel pango-devel
# Follow the steps above to create the RPM
```

Note: the code above also works for Fedora 37.
